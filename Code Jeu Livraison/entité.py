from msilib.schema import Font
import pygame
from pathfinding.core.grid import Grid
from pathfinding.finder.a_star import AStarFinder
import random
import time
import os


pygame.mixer.init()

#Chemin pour les musiques
music_files = ["musique1.mp3", "musique2.mp3", "musique3.mp3"]


# Liste des fichiers audio dans le répertoire
music_file = random.choice(music_files)


# Charger une musique aléatoire
pygame.mixer.music.load(music_file)



# Jouer la musique en boucle
pygame.mixer.music.play(-1)

#Couleurs 
RED = (255, 0, 0)
BLUE = (0, 0, 255)
WHITE = (255, 255, 255)


#classes 
def move_towards(current, target, velocity):
    if current < target:
        return min(current + velocity, target)
    elif current > target:
        return max(current - velocity, target)
    else:
        return current
def grid_to_screen(x, y, cell_width, cell_height):
    return x * cell_width, y * cell_height


#Images
background_image = "./Carte.png"
player = pygame.image.load(r'agent.png')
player2 = pygame.image.load(r'agent2.png')
start_button_image = pygame.image.load("start_button.png")
stop_button_image = pygame.image.load("stop_button.png")


#initialisation pygame
pygame.init()
screen = pygame.display.set_mode((900, 900))
pygame.display.set_caption("Simulation Hackaton - Technique")
background = pygame.image.load(background_image).convert()


#Coordonnées positions et vélocité
x, y = 740, 650
x2, y2 = 710, 650
velocity = 0.25


#cibles pour les agents( robot )
targets = [(710, 490), (485, 492), (440, 250), (188, 574), (152, 754)]
target_x, target_y = targets[0]
target_index = 1

targets2 = [(680, 470), (465, 472), (420, 230), (168, 554), (132, 734)]
target_x2, target_y2 = targets2[0]
target_index2 = 1


#prix livraisons et total 
price2 = random.randint(5, 50)
price = random.randint(5, 50)

at_target = False
deliveries = 0
total_earnings = 0

#Battery
battery_timer1 = 150
recharge_timer1 = 0
battery_timer2 = 150
recharge_timer2 = 0
recharge_duration = 10


# Dessin du score board
scoreboard_rect1 = pygame.Rect(10, 20, 200, 130)
scoreboard_rect2 = pygame.Rect(690, 20, 200, 130)
pygame.draw.rect(screen, BLUE, scoreboard_rect1, 2)
pygame.draw.rect(screen, BLUE, scoreboard_rect2, 2)
font = pygame.font.SysFont("Arial", 18, bold=False)


# Ajout des boutons Start et Stop
button_start = pygame.Rect(350, 20, 100, 50)
button_stop = pygame.Rect(470, 20, 100, 50)

button_start = pygame.Rect(220, 15, start_button_image.get_width(), start_button_image.get_height())
button_stop = pygame.Rect(460, 20, stop_button_image.get_width(), stop_button_image.get_height())

font_button = pygame.font.Font(None, 24)


# Affichage du texte
score_text1 = font.render("Nombre de livraisons : " + str(deliveries), True, WHITE)
price_text1 = font.render("Prix des livraisons : " + str(price) + " €", True, WHITE)
earnings_text1 = font.render("Gains totaux : " + str(total_earnings) + " €", True, WHITE)

score_text2 = font.render("Agent 2 - Livraisons : " + str(deliveries), True, WHITE)
price_text2 = font.render("Agent 2 - Prix : " + str(price) + " €", True, WHITE)
earnings_text2 = font.render("Agent 2 - Gains : " + str(total_earnings) + " €", True, WHITE)


# Scoreboard agent 1
score_rect1 = score_text1.get_rect()
price_rect1 = price_text1.get_rect()
earnings_rect1 = earnings_text1.get_rect()
score_rect1.top = scoreboard_rect1.top + 10
score_rect1.left = scoreboard_rect1.left + 10
price_rect1.top = score_rect1.bottom + 10
price_rect1.left = scoreboard_rect1.left + 10
earnings_rect1.top = price_rect1.bottom + 10
earnings_rect1.left = scoreboard_rect1.left + 10


# Scoreboard agent 2
score_rect2 = score_text2.get_rect()
price_rect2 = price_text2.get_rect()
earnings_rect2 = earnings_text2.get_rect()
score_rect2.top = scoreboard_rect2.top + 10
score_rect2.left = scoreboard_rect2.left + 10
price_rect2.top = score_rect2.bottom + 10
price_rect2.left = scoreboard_rect2.left + 10
earnings_rect2.top = price_rect2.bottom + 10
earnings_rect2.left = scoreboard_rect2.left + 10


#Affichage scoreboard agent 1
screen.blit(score_text1, score_rect1)
screen.blit(price_text1, price_rect1)
screen.blit(earnings_text1, earnings_rect1)


#Affichage scoreboard agent 2
screen.blit(score_text2, score_rect2)
screen.blit(price_text2, price_rect2)
screen.blit(earnings_text2, earnings_rect2)


grid = Grid(matrix=[[1 for x in range(100)] for y in range(100)])
finder = AStarFinder()

#Boutons Play and Pause
start_text = font_button.render("", True, (255, 255, 255))
stop_text = font_button.render("", True, (255, 255, 255))


running = False


#Boucle du Jeu
while True:
    screen.blit(background, (0, 0))
    screen.blit(player, (x, y))
    screen.blit(player2, (x2, y2))
    screen.blit(score_text1, score_rect1)
    screen.blit(price_text1, price_rect1)
    screen.blit(earnings_text1, earnings_rect1)
    
    pygame.draw.rect(screen, BLUE, scoreboard_rect1, 2)
    pygame.draw.rect(screen, RED, scoreboard_rect2, 2)

    screen.blit(score_text2, score_rect2)
    screen.blit(price_text2, price_rect2)
    screen.blit(earnings_text2, earnings_rect2)

    # Add the battery status display code here
    battery_text1 = font.render("Batterie : " + str(int(battery_timer1)) + " s", True, WHITE)
    battery_rect1 = battery_text1.get_rect()
    battery_rect1.top = earnings_rect1.bottom + 10
    battery_rect1.left = scoreboard_rect1.left + 10
    screen.blit(battery_text1, battery_rect1)

    battery_text2 = font.render("Batterie : " + str(int(battery_timer2)) + " s", True, WHITE)
    battery_rect2 = battery_text2.get_rect()
    battery_rect2.top = earnings_rect2.bottom + 10
    battery_rect2.left = scoreboard_rect2.left + 10
    screen.blit(battery_text2, battery_rect2)

    # Continue with the rest of your code
    screen.blit(start_button_image, button_start)
    screen.blit(stop_button_image, button_stop)
    screen.blit(start_text, (button_start.x + 20, button_start.y + 14))
    screen.blit(stop_text, (button_stop.x + 20, button_stop.y + 14))


    #Fonctionnement Play and Pause Souris 
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            quit()
        # Gestion des clics sur les boutons Start et Stop
        if event.type == pygame.MOUSEBUTTONDOWN:
            mouse_x, mouse_y = event.pos
            if button_start.collidepoint(mouse_x, mouse_y):
                running = True
            elif button_stop.collidepoint(mouse_x, mouse_y):
                running = False
            
            
    if running:
    # Update battery timers and manage agent behavior
        if recharge_timer1 <= 0:
            battery_timer1 -= 0.0050  # Reduce battery_timer1 by 1/60 for each frame in 60 FPS
            if battery_timer1 <= 0:  # If battery is empty
                target_x, target_y = 740, 650  # Set target to spawn point
                recharge_timer1 = recharge_duration  # Start recharge timer
        else:
            recharge_timer1 -= 0.1  # Reduce recharge_timer1 by 1/60 for each frame in 60 FPS
            if recharge_timer1 <= 0:
                battery_timer1 = 150  # Recharge the battery

        if recharge_timer2 <= 0:
            battery_timer2 -= 0.0050  # Reduce battery_timer2 by 1/60 for each frame in 60 FPS
            if battery_timer2 <= 0:  # If battery is empty
                target_x2, target_y2 = 710, 650  # Set target to spawn point
                recharge_timer2 = recharge_duration  # Start recharge timer
        else:
            recharge_timer2 -= 0.0050  # Reduce recharge_timer2 by 1/60 for each frame in 60 FPS
            if recharge_timer2 <= 0:
                battery_timer2 = 150  # Recharge the battery
    
    if running and recharge_timer1 <= 0:
        
        
        # Jeu agent 1
        if x == target_x and y == target_y:
            total_earnings += price
            if not at_target:
                at_target = True
                time.sleep(2)
                if target_index == 0:
                    random.shuffle(targets)
                target_x, target_y = targets[target_index]
                target_index = (target_index + 1) % len(targets)
                price = random.randint(5, 15)  # New random price between 5 and 15
                price_text1 = font.render("Prix des livraisons : " + str(price) + " €", True, WHITE) # Update the price text
            else:
                at_target = False
                target_x, target_y = 730, 640
                deliveries += 1
                score_text1 = font.render("Nombre de livraisons : " + str(deliveries), True, (255, 255, 255))
                earnings_text1 = font.render("Gains totaux : " + str(total_earnings) + " €", True, (255, 255, 255))

        x = move_towards(x, target_x, velocity)
        y = move_towards(y, target_y, velocity)
        
    pygame.display.update()
    
    
    if running and recharge_timer2 <= 0:
    # Jeu agent 2
        if x2 == target_x2 and y2 == target_y2:
            total_earnings += price2
            if not at_target:
                at_target = True
                time.sleep(2)
                if target_index2 == 0:
                    random.shuffle(targets2)
                target_x2, target_y2 = targets2[target_index2]
                target_index2 = (target_index2 + 1) % len(targets2)
                price2 = random.randint(5, 15)  # New random price between 5 and 15
                price_text2 = font.render("Prix des livraisons : " + str(price2) + " €", True, WHITE) # Update the price text
            else:
                at_target = False
                target_x2, target_y2 = 700, 600
                deliveries += 1
                score_text2 = font.render("Nombre de livraisons : " + str(deliveries), True, (255, 255, 255))
                earnings_text2 = font.render("Gains totaux : " + str(total_earnings) + " €", True, (255, 255, 255))

        x2 = move_towards(x2, target_x2, velocity)
        y2 = move_towards(y2, target_y2, velocity)

    pygame.display.update()


pygame.mixer.music.stop()