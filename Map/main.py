background_image = "./Carte.png"

import pygame

pygame.init()
screen = pygame.display.set_mode((900, 900))

pygame.display.set_caption("Simulation Hackaton - Technique")
background = pygame.image.load(background_image).convert()
player = pygame.image.load(r'robot.jpg')
x = 100
y = 100
velocity = 12

run = True
while run:
        
        screen.blit(background, (0, 0))
        screen.blit(player, (x, y))
        
        for event in pygame.event.get():
                if event.type == pygame.QUIT:
                        run = False
                if event.type == pygame.KEYDOWN:
                        if event.key == pygame.K_LEFT:
                                x -= velocity
                        if event.key == pygame.K_RIGHT:
                                x += velocity
                        if event.key == pygame.K_UP:
                                y -= velocity
                        if event.key == pygame.K_DOWN:
                                y += velocity

        pygame.display.update()

pygame.quit()
quit()